% Clear Workspace
clear

% Input Files
source_path = 'GT02-source.png';
trimap_path = 'GT02-trimap.png';
output_path = 'GT02-output-a.png';
fg_path = 'GT02-output-fg.png';
bg_path = 'GT02-output-bg.png';

% --------------------------------------
fprintf('Starting...\n');

% Load Images
source = imread(source_path);
trimap = imread(trimap_path);

fprintf('Images read\n');

[h,w,~]=size(source);

% Parameters
knn = 16;
lambda = 0.05;
feat_xy_scale = 0.2 / max(w, h);

% Run
alpha = knn_matting(source, trimap, knn, lambda, feat_xy_scale);

% Show & Save Images
view_result(source, alpha, output_path, fg_path, bg_path);
