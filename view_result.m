% Define View Function
function [] = view_result(source, alpha, output_path, fg_path, bg_path)
  figure, imshow(alpha);
  s = single(source)/255;
  fg = s .* repmat(alpha,[1,1,3]);
  figure, imshow(fg);
  figure, imshow(s - fg);
  
  if nargin > 2
    imwrite(alpha, output_path);
    imwrite(fg, fg_path);
    imwrite(s - fg, bg_path);
  end
end
