% Define Function
function [alpha] = knn_matting(source, trimap, knn, lambda, xy_scale)

%------------------------------------------------------------

% convert trimap to greyscale
if size(trimap, 3) ~= 1
  trimap = rgb2gray(trimap);
end

% Get Image Size
[h,w,~]=size(source);
npixels = w * h;

% Check if the size of 'source' image matches 'trimap' image
%if size(source) ~= size(trimap)
%  err = MException('Argument', 'size(source) != size(trimap)');
%  throw(err);
%end

%------------------------------------------------------------

% Feature Vectors
feat = zeros(npixels, 6);
hsv = rgb2hsv(source);
for i = 1:h
  for j = 1:w
    index = (i-1) * w + j;
    hue = hsv(i,j,1);
    
    feat(index, 1) = cos(hue);
    feat(index, 2) = sin(hue);
    feat(index, 3) = hsv(i,j,2); % S
    feat(index, 4) = hsv(i,j,3); % V
    feat(index, 5) = j * xy_scale; % X
    feat(index, 6) = i * xy_scale; % Y
  end
end
clear hsv

fprintf('Features is created\n');

%------------------------------------------------------------

% KNN Search
[idx, dist] = knnsearch(feat, feat, 'K', knn, 'NSMethod', 'kdtree');
clear feat

fprintf('KNN is done\n');

%------------------------------------------------------------

% Prepare to create sparse matrices A and D

i1 = zeros(1, npixels * knn);
j1 = zeros(1, npixels * knn);
s1 = zeros(1, npixels * knn);
s2 = zeros(npixels, 1);
for index = 1:npixels
  off = (index-1) * knn;
  d = dist(index,:);
  nd = max(d);
  sd = sum(d);
  i1(off+1:off+knn) = repmat(index, knn, 1);
  j1(off+1:off+knn) = idx(index,:);
  s1(off+1:off+knn) = 1 - d / nd;
  s2(index) = knn - sd / nd;
end
clear idx dist

fprintf('creation of sparse matrices is prepared\n');

A = sparse(i1,j1,s1,npixels,npixels);
clear i1 j1 s1

fprintf('sparse matrix A is created\n');

D = spdiags(s2, 0, npixels, npixels);
clear s2

fprintf('sparse matrix D is created\n');

%------------------------------------------------------------

% Laplacian Matrix
L = D - A;
clear D A
%L = L.' * L;

fprintf('laplacian matrix L is created\n');

% Marked Array
m = zeros(npixels, 1);
v = zeros(npixels, 1);
for i = 1:h
  for j = 1:w
    index = (i-1) * w + j;
    c = trimap(i,j,1);
    
    if c == 255
      m(index) = 1;
      v(index) = 1;
    elseif c == 0
      m(index) = 1;
    end
  end
end

% Create Linear System
D = spdiags(m, 0, npixels, npixels);
clear m
A = L + lambda * D;
clear L D

% Solve Linear System
x = pcg(A, lambda * v, [], 100);
clear A v

%------------------------------------------------------------

% Generate Alpha
alpha = zeros(h, w);
for i = 1:h
  for j = 1:w
    index = (i-1) * w + j;
    alpha(i,j) = x(index);
  end
end
alpha = min(1.0, max(0.0, alpha));

% end of function
end

%------------------------------------------------------------
%------------------------------------------------------------
%------------------------------------------------------------
