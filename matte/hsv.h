#pragma once

#include <cmath>
#include <algorithm>

namespace
{
  inline 
  void _HSV2RGB(float h, float s, float v, float& r, float& g, float& b)
  {
    const float hx = h / 60.0f;
    float hi;
    const float f = std::modf(hx, &hi);
    //const float hi = std::floor(hx);
    //const float f = hx - hi;
    
    const float p = v * (1-s), q = v * (1-f*s), t = v * (1-(1-f)*s);
    
    switch (((int)hi)%6)
    {
      case 0:
        r = v, g = t, b = p;
        break;
      case 1:
        r = q, g = v, b = p;
        break;
      case 2:
        r = p, g = v, b = t;
        break;
      case 3:
        r = p, g = q, b = v;
        break;
      case 4:
        r = t, g = p, b = v;
        break;
      case 5:
        r = v, g = p, b = q;
        break;
    }
  }
  
  inline 
  void _RGB2HSV(int r, int g, int b, float& h, float& s, float& v)
  {
    const int M = std::max(r, std::max(g, b));
    const int m = std::min(r, std::min(g, b));
    const int C = M - m;
    const int V = M;
    
    float h1;
    
    if (M == m)
      h1 = 0.0f;
    else if (M == r)
      h1 = std::fmod(float(g-b)/C, 6.0f);
    else if (M == g)
      h1 = float(b-r)/C + 2;
    else if (M == b)
      h1 = float(r-g)/C + 4;
    else
      h1 = 0.0f;
    
    h = 60.0f * h1;
    s = (V == 0 ? 0.0f : (float) C / (float) V );
    v = (float) V / 255.0f;
  }
  
}

