#include <Magick++.h>
#include <iostream>
#include <cstdlib>

// usage
static void usage(const char* pname)
{
  std::cerr << "usage:"<<(pname)<<": [options] <source> <output>\n";
  std::cerr << "options:\n";
  std::cerr << "   -a <alpha>        : Set Alpha Mask\n";
  std::cerr << "   -n                : Invert alpha mask\n";
  std::cerr << "   -h                : Show help\n";
  std::cerr << std::endl;
}

// options
const char *srcPath = NULL, *mskPath = NULL, *dstPath = NULL;
bool invertAlphaMask = false;

// parse options
static void parseOpt(int argc, char* argv[])
{
  int i;
  
  // parse options
  for (i = 1; i < argc; ++i)
  {
    if (argv[i][0] == '-') // option
    {
      switch (argv[i][1])
      {
        case 'h':
          usage(argv[0]);
          std::exit(0);
          return;
          
	case 'a':
          mskPath = argv[++i];
          if (mskPath == NULL)
          {
            std::cerr << argv[0] << ": where's the path to mask?\n";
            std::exit(1);
          }
          break;
          
        case 'n':
          invertAlphaMask = true;
          break;
          
        default:
          std::cerr << argv[0] << ": invalid option = " << argv[i] << "\n";
          std::exit(1);
          return;
      }
    }
    else
      break;
  }
  
  if (i+2 == argc)
  {
    srcPath = argv[i];
    dstPath = argv[i+1];
  }
  else
  {
    std::cerr << argv[0] << ": Invalid arguments ( " << argv[0] << " -h )\n";
    std::exit(1);
  }
}


int main(int argc, char* argv[])
{
  //
  parseOpt(argc, argv);
  
  // Initialize ImageMagick
  Magick::InitializeMagick(*argv);
  
  try
  { // BEGIN
    Magick::Image srcImg(srcPath);
    
    if (mskPath != NULL)
    {
      Magick::Image mskImg(mskPath);
      
      if (invertAlphaMask)
        mskImg.negate();
      
      srcImg.composite(mskImg, 0, 0, Magick::CopyOpacityCompositeOp);
    }
    
    srcImg.write(dstPath);
  } // END
  catch (Magick::Exception & err) // Caught ImageMagick Error
  {
    std::cerr << "Magick Exception: " << err.what() << std::endl;
    return 1;
  }
  
  return 0;
}
