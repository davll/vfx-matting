#include "matting.h"
#include "hsv.h"

extern "C"
{
#include "cs/cs.h"
}

#include <opencv2/flann/flann.hpp>

#include <cassert>
#include <cmath>
#include <algorithm>
#include <vector>

#ifdef DEBUG
#include <fstream>
#endif // DEBUG

cv::Mat matte_knn(cv::Mat source, cv::Mat trimap)
{
  // Initialized Defs
  const cv::Size isize = source.size();
  const int npixels = isize.area();
  
  // Parameters
  const float f_xyscale = 16.0f / std::max(isize.width, isize.height);
  //const float f_xyscale = 0.0f;
  const int knn = 4;
  const float lambda = 2.0f;
  
#ifdef DEBUG
  std::ofstream dbg_feature("features.txt", 
                            std::ios_base::trunc | std::ios_base::out);
#endif // DEBUG
  
  // Generate Feature Vectors
  cv::Mat_<float> features(npixels, 6);
  {
    for (int w = isize.width, h = isize.height, i = 0; i < h; ++i)
      for (int j = 0; j < w; ++j)
      {
        // Index of the pixel
        const int index = i * w + j;

	// Fetch Color
        const float r = source.at<cv::Vec3b>(i, j)[0];
        const float g = source.at<cv::Vec3b>(i, j)[1];
        const float b = source.at<cv::Vec3b>(i, j)[2];
        float h, s, v;
        _RGB2HSV(r, g, b, h, s, v);
        
	// Generate Feature Vector
        features(index, 0) = std::cos(h/180.0f * 3.14159f);
        features(index, 1) = std::sin(h/180.0f * 3.14159f);
        features(index, 2) = s;
        features(index, 3) = v;
        features(index, 4) = j * f_xyscale;
        features(index, 5) = i * f_xyscale;
        
#ifdef DEBUG
        dbg_feature << "(" << j << ", " << i << ") : ";
        for (int _i = 0; _i < 6; ++_i)
          dbg_feature << features(k, _i) << " ";
        dbg_feature << "\n";
#endif // DEBUG
      }
  }
  
  // Build Nearest Neighbor Database
  cv::flann::Index_<float> featDB(features, cvflann::KDTreeIndexParams(4));
  
  // A. Create Laplacian Matrix L = D - A
  //   
  //   1) Sparse Matrix Aij = k(i,j)
  //   2) Diagonal Matrix Dij = sum(j wrt i){k(i,j)}
  // 
  cs * matL = cs_spalloc(npixels, npixels, npixels*(knn+1), 1, 0);
  assert(matL != NULL);
  {
    std::vector<float> query(6);
    std::vector<int> indices(knn);
    std::vector<float> dists(knn);
    
    for (int index = 0; index < npixels; ++index)
    {
      query = features.row(index);
      
      // KNN search
      featDB.knnSearch(query, indices, dists, knn, 16);
      
      //printf("%d: %d %d\n", index, indices[0], indices[1]);
      
      // Analysis distances
      float max_dist = 0.0f, sum_dist = 0.0f;
      for (int jp = 0; jp < knn; ++jp)
      {
        float d = dists[jp];
        max_dist = std::max(max_dist, d);
        sum_dist += d;
      }
      
      // Compute Dij
      float sum_k = knn - sum_dist / max_dist;
      float val_Lii = sum_k;
      
      // Compute -Aij
      for (int jp=0; jp < knn; ++jp)
      {
        int j = indices[jp];
        float d = dists[jp];
        float k = 1.0f - d / max_dist;
        if (index != j)
        {
          cs_entry(matL, index, j, -k);
        }
        else
          val_Lii -= k;
      }
      
      // Store Dii - Aii
      cs_entry(matL, index, index, val_Lii);
    }
    
    //cs * matL_tr = cs_transpose(matL, 1);
    //cs * matL_new = cs_multiply(matL_tr, matL);
    //cs_spfree(matL_tr);
    //cs_spfree(matL);
    //matL = matL_new;
  }
  
  // B. Create The Linear System:
  //    
  //    matA * X = vecB
  //    
  //    (L + \lambda * diag(m)) \alpha = v
  //    m: marked, v: marked as foreground
  //
  cs * matA;
  std::vector<double> vecB(npixels);
  {
    cs * matD = cs_spalloc(npixels, npixels, npixels, 1, 0);
    assert(matD != NULL);
    
    for (int w = isize.width, h = isize.height, y=0; y<h; ++y)
      for (int x=0; x<w; ++x)
      {
        int index = y * w + x;
        
        // Fetch Trimap Color
	const int colour = trimap.at<unsigned char>(y, x);
        
        // Compute Dij
        switch (colour)
	{
	  case 255:
	    vecB[index] = lambda;
	  case 0:
	    cs_entry(matD, index, index, lambda);
	    break;
	}
      }
    
    matA = cs_add(matL, matD, 1.0, 1.0);
    assert(matA != NULL);
    
    cs_spfree(matD);
  }
  cs_spfree(matL); matL = NULL;
  
  // Solve The Linear System
  std::vector<double>& vecX = vecB;
  {
    //if (!cs_cholsol(1, matA, &vecX.front()))
    if (!cs_lusol(2, matA, &vecX.front(), 0.0f))
      std::cerr << "Cannot solve the linear system\n";
  }
  cs_spfree(matA); matA = NULL;
  
  // Generate Alpha Map
  cv::Mat_<unsigned char> alpha(isize);
  {
    for (int w = isize.width, h = isize.height, i = 0; i < h; ++i)
      for (int j = 0; j < w; ++j)
      {
        // Index of the pixel
        const int index = i * w + j;
	
	int val = vecX[index] * 255;
	val = std::max(0, val);
	val = std::min(255, val);
	
        alpha(i, j) = val;
      }
  }
  
  // Return The Alpha Map
  return alpha;
}

