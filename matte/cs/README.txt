CSparse/Source directory: primary ANSI C source code files for CSparse.
All of these files are printed verbatim in the book.  To compile the
libcsparse.a C-callable library, just type "make" in this directory.

Timothy A. Davis, http://www.suitesparse.com

http://people.sc.fsu.edu/~jburkardt/c_src/csparse/csparse.html
http://www.cise.ufl.edu/research/sparse/CSparse/

