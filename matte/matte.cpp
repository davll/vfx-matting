#include "matting.h"

//#include <Magick++.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <cstdlib>
#include <cstring>

// usage
static void usage(const char* pname)
{
  std::cerr << "usage: "<<(pname)<<": [options] <source> <trimap> <output>\n";
  std::cerr << "options:\n";
  std::cerr << "   -help           : Show help\n";
  std::cerr << "   -h              : (alias of -help)\n";
  
  // MATTING MODE
  std::cerr << "   -mode <mode>    : Select mode\n";
  std::cerr << "   -m    <mode>    : (alias of -mode)\n";
  std::cerr << "         bayesian  :  1) use Bayesian Matting\n";
  std::cerr << "         knn       :  2) use KNN Matting\n";
  
  std::cerr << std::endl;
}

// MATTING MODE
enum MattingMode
{
  MATTING_BAYESIAN,
  MATTING_KNN
};

// options
const char* sourcePath = NULL;
const char* trimapPath = NULL;
const char* outputPath = NULL;
MattingMode mattingMode = MATTING_BAYESIAN;

// option mode
static inline void parseOptMode(int i, char* argv[])
{
  const char* mode = argv[++i];
  
  if (mode == NULL)
  {
    std::cerr << argv[0] << ": -m mode ... where's the \"mode\"?\n";
    std::exit(1);
  }
  // MATTING MODE
  else if (std::strcmp(mode, "bayesian")==0)
    mattingMode = MATTING_BAYESIAN;
  else if (std::strcmp(mode, "knn")==0)
    mattingMode = MATTING_KNN;
  else
  {
    std::cerr << argv[0] << ": Invalid matting mode\n";
    std::exit(1);
  }
}

// parse options
static void parseOpt(int argc, char* argv[])
{
  int i;

  // parse opts
  for (i = 1; i < argc; ++i)
  {
    const char* opt = argv[i];
    
    if (std::strcmp(opt, "-h")==0 || std::strcmp(opt, "-help")==0)
    {
      usage(argv[0]);
      std::exit(0);
    }
    else if (std::strcmp(opt, "-m")==0 || std::strcmp(opt, "-mode")==0)
    {
      parseOptMode(i, argv);
      ++i;
    }
    else if (opt[0] == '-')
    {
      std::cerr << argv[0] << ": invalid option = " << opt << "\n";
      std::exit(1);
    }
    else
      break;
  }
  
  // Get Pathes
  if (i+3 == argc)
  {
    sourcePath = argv[i];
    trimapPath = argv[i+1];
    outputPath = argv[i+2];
  }
  else
  {
    std::cerr << argv[0] << ": Invalid arguments ( " << argv[0] << " -h )\n";
    std::exit(1);
  }
}

int main(int argc, char* argv[])
{
  //
  parseOpt(argc, argv);
    
  // Load Source Image
  cv::Mat source = cv::imread(sourcePath);
  if (source.empty())
  {
    std::cerr << argv[0] << ": fail to read source = "<<(sourcePath)<<"\n";
    std::exit(1);
  }
  
  // Load Trimap Image
  cv::Mat trimap = cv::imread(trimapPath, 0);
  if (trimap.empty())
  {
    std::cerr << argv[0] << ": fail to read source = "<<(sourcePath)<<"\n";
    std::exit(1);
  }
  
  // Check if the sizes are the same
  {
    cv::Size s = source.size(), t = trimap.size();
    if (s.width != t.width || s.height != t.height)
    {
      std::cerr << argv[0] << ": size of the source must match the trimap\n";
      std::exit(1);
    }
  }
  
  // Output Image Object
  cv::Mat output;
  
  // MATTING MODE
  switch (mattingMode)
  {
    case MATTING_BAYESIAN: // Bayesian Matting
      output = matte_bayesian(source, trimap);
      break;
    case MATTING_KNN: // KNN Matting
      output = matte_knn(source, trimap);
      break;
  }
  
  cv::imwrite(outputPath, output);
  
  return 0;
}
