#pragma once

#include "hsv.h"

#include <opencv2/core/core.hpp>

#include <cmath>

// Bayesian Matting
cv::Mat matte_bayesian(cv::Mat source, cv::Mat trimap);

// KNN Matting
cv::Mat matte_knn(cv::Mat source, cv::Mat trimap);

